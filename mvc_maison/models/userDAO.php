<?php 

	require_once(PATH_MODELS.'DAO.php');

	class userDAO extends DAO
	{
		public function userDAO(){
			$res = $this->queryRow('SELECT idAccessPage FROM User where Nom = ?' array($_GET['id']));
			if($res)
			{
				require_once(PATH_ENTITY.'User.php');
				return $res;
			}
			else return null;
		}
	}